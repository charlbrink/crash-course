import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ngfor',
  templateUrl: './ngfor.component.html',
  styleUrls: ['./ngfor.component.css']
})
export class NgforComponent {

  courses = [
    { id: 1, name: 'course1' },
    { id: 2, name: 'course2' },
    { id: 3, name: 'course3' }
  ];

  onAdd() {
    const nextCourseNumber = this.courses.length + 1;
    const nextCourseName = 'course' + nextCourseNumber;
    this.courses.push({ id: nextCourseNumber, name: nextCourseName});
  }

  onDelete(course) {
    const index = this.courses.indexOf(course);
    this.courses.splice(index, 1);
    console.log('Deleting ', course);
  }

  onChange(course) {
    course.name = 'UPDATED';
  }

  trackCourse(index, course) {
    return course ? course.id : undefined;
  }
}
