import { Component, OnInit } from '@angular/core';
import { CourseService } from '../course.service';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {
  private title = 'List of courses';
  courses;
  isActive = true;
  surname = 'Smith';
  email = 'me@domain.com';
  course = {
    title:  'Angular crash course',
    rating: 4.975,
    students: 30000,
    price: 190.95,
    releaseDate: new Date(2018, 3, 1),
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt'
  };

  constructor(service: CourseService) {
    this.courses = service.getCourses();
  }

  ngOnInit() {
  }

  getTitle() {
    return this.title;
  }

  onSave($event) {
    $event.stopPropagation();
    console.log('Button was clicked', $event);
  }

  onDivClicked() {
    console.log('Div was clicked');
  }

  onNameEnter(name) {
    console.log('Name:' + name);
  }

  onSurnameEnter(surname) {
    console.log('Surname:' + surname);
  }

  onEmailEnter(email) {
    console.log('Email:' + email);
  }
}
