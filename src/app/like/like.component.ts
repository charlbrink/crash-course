import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-like',
  templateUrl: './like.component.html',
  styleUrls: ['./like.component.css']
})
export class LikeComponent implements OnInit {

  @Input('likesCount') count: number;
  @Input('isActive') isLiked: boolean;
  @Output() change = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onClick() {
    // this.count += (!this.isLiked) ? 1 : -1;
    if (!this.isLiked) {
      this.count++;
    } else {
      if (this.count > 0) {
        this.count--;
      }
    }
    this.isLiked = !this.isLiked;
    this.change.emit({ isLiked: this.isLiked, count: this.count});
  }
}

export interface LikeClickedEvent {
  isLiked: boolean;
  count: number;
}
