import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-favourite',
  templateUrl: './favourite.component.html',
  styleUrls: ['./favourite.component.css'],
  styles: [
    `
    .fa {
      color: green;
  }
    `
  ]
})
export class FavouriteComponent implements OnInit {

  @Input('favourite') favourite = false;
  @Output() change = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  toggle() {
    this.favourite = !this.favourite;
    this.change.emit({ newValue: this.favourite});
  }
}

export interface FavouriteChangedEventArgs {
  newValue: boolean;
}

