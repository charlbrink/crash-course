import { Component } from '@angular/core';
import { FavouriteChangedEventArgs } from './favourite/favourite.component';
import { LikeClickedEvent } from './like/like.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  post = {
    title: 'Blah',
    isFavourite: true
  };

  tweet = {
    body: 'Here is the body of a tweet',
    isLiked: false,
    likesCount: 10
  };

  onFavouriteChanged(eventArgs: FavouriteChangedEventArgs) {
    console.log('Favourite changed:', eventArgs.newValue);
  }

  onLikeClicked(eventArgs: LikeClickedEvent) {
    console.log('tweet was liked: ', eventArgs.isLiked, '; Number of likes: ', eventArgs.count);
  }
}
