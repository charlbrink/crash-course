import { Component, OnInit } from '@angular/core';
import { AuthorService } from '../author.service';

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.css']
})
export class AuthorComponent implements OnInit {
  private _authors: string[];

  constructor(service: AuthorService) {
    this._authors = service.getAuthors();
  }

  ngOnInit() {
  }

  get authors() {
    return this._authors;
  }

}
