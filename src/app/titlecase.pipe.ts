
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'titlecase'
})
export class TitlecasePipe implements PipeTransform {
    PREPOSITIONS: string[] = ['of', 'the'];

    private isPreposition(value: string): boolean {
        return this.PREPOSITIONS.includes(value);
    }

    private toTitleCase(word: string): string {
        return word.substr(0, 1).toUpperCase() + word.substr(1);
    }
    
    transform(value: string): any {
        if (!value) {
            return null;
        }
        const words = value.toLowerCase().split(' ');
        for (let i = 0; i < words.length; i++) {
            if (i === 0 || !this.isPreposition(words[i])) {
                words[i] = this.toTitleCase(words[i]);
            }
        }
        return words.join(' ');
    }

}
