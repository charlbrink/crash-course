import { Directive, HostListener, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[appInputFormat]'
})
export class InputFormatDirective {
  @Input('appInputFormat') appInputFormat;

  constructor(private el: ElementRef) { }

  @HostListener('focus')
  onFocus() {
    console.log('on focus');
  }

  @HostListener('blur')
  onBlur() {
    const value: string = this.el.nativeElement.value;
    if (this.appInputFormat === 'uppercase') {
      this.el.nativeElement.value = value.toUpperCase();
    } else {
      this.el.nativeElement.value = value.toLowerCase();
    }
    console.log('on blur');
  }

}
