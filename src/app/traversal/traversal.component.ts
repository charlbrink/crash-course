import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-traversal',
  templateUrl: './traversal.component.html',
  styleUrls: ['./traversal.component.css']
})
export class TraversalComponent {

task = {
  title: 'Review applications',
  assignee: {
    name: 'John Smith'
  }
};

unassignedTask = {
  title: 'Review applications',
  assignee: null
};

}
